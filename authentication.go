package main

import (
	"context"
	"net/http"

	"bitbucket.com/zrunyan/go-api/indexes"
)

/**
 Authentication Middleware
**/

type UserCredentials struct {
	username      string
	password      string
	session_token string
	apikey        string
}

func Authentication(ctx context.Context, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		Logger := indexes.LoggerFromContext(ctx)
		ReqID := indexes.ReqIdFromcontext(r.Context())

		/* Lets get possible credentials that could be passed in */
		// Lets start going through each possible auth until we get one that matches
		// else stop the request and immediately return 401. A Username is required in either
		// the header, the url, or the cookie(to be added) and will return 400 if not found
		//
		// Apikey -> session token -> Username/Password

		// Lets get all possible credentials passed into the request
		auth := getCredentialsFromRequest(ctx, r)

		if auth.username == "" {

			// If no username is passed in the request, return 400
			// We cannnot identify this user
			Logger.Error(ReqID, ": Stopping request, no username found")
			w.WriteHeader(401)
			return

		}

		if auth.apikey != "" {

			// Run apikey against database here

			// This will be the database
			storedApiKey := "12345"

			if auth.apikey == storedApiKey {
				// get TokenUser
				tokenUser := indexes.TokenUser{"exampleUserId"}

				Logger.Debug(ReqID, "Request Authenticated for user", tokenUser.UserId)

				ReqCtx := context.WithValue(r.Context(), "tokenUser", tokenUser)

				next.ServeHTTP(w, r.WithContext(ReqCtx))
				return

			}

		}

		if auth.session_token != "" {

			// See if there is an existing token that matches the one provided
			// This would actually be the database
			storedToken := "12345678"

			// If we have a valid token that is not expired,
			// Lets add all needed user information to context
			// as tokenUser

			if auth.session_token == storedToken {

				// get TokenUser
				tokenUser := indexes.TokenUser{"exampleUserId"}

				ReqCtx := context.WithValue(r.Context(), "tokenUser", tokenUser)
				Logger.Debug(ReqID, "Request Authenticated for user", tokenUser.UserId)
				next.ServeHTTP(w, r.WithContext(ReqCtx))
				return

			}
		}

		// If we have no successful authorization, return 401 forbidden
		w.WriteHeader(401)

	})
}
