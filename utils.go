package main

import (
	"context"
	"net/http"
	"net/http/httputil"
	"net/url"

	"bitbucket.com/zrunyan/go-api/indexes"

	"github.com/gorilla/mux"
)

// // Opens and checks the database connection
// func GetDB(config Config) (*sql.DB, error) {

// 	// Lets Open the connection to the db
// 	var db, err = sql.Open("postgres", config.PSQLInfo)
// 	if err != nil {
// 		return db, err
// 	}

// 	// Lets check the connection to the db
// 	err = db.Ping()
// 	if err != nil {
// 		return db, err
// 	}

// 	return db, nil
// }

/* Parses a query from URL string */
func getURLQuery(ctx context.Context, fullUrl string) (query url.Values) {

	// Get logger
	Logger := indexes.LoggerFromContext(ctx)

	// Lets parse the URL
	u, err := url.Parse(fullUrl)
	if err != nil {
		Logger.Error("Error parsing URL string")
	}

	// Lets get the query
	query, err = url.ParseQuery(u.RawQuery)
	if err != nil {
		Logger.Error("Error parsing URL Query")
	}

	// Lets return the query
	return query
}

func dumpRequestInfo(ctx context.Context, r *http.Request) string {

	Logger := indexes.LoggerFromContext(ctx)

	// Lets use the httputil to get all information
	reqInfo, err := httputil.DumpRequest(r, true)
	if err != nil {
		Logger.Error("Error dumping request:", err)
	}

	// Return the dump as a string
	return string(reqInfo)
}

func getCredentialsFromRequest(ctx context.Context, r *http.Request) UserCredentials {
	// This function will get any credentials passed in with a request
	// and return a Credentials struct

	// Lets get all the possible credentials passed in

	// URL query
	query := getURLQuery(ctx, r.URL.String())

	apikeyFromQuery := query.Get("apikey")
	usernameFromQuery := query.Get("username")

	// Request Headers
	tokenFromHeader := r.Header.Get("Token")
	apikeyFromHeader := r.Header.Get("Authorization")
	passwordFromHeader := r.Header.Get("Password")
	usernameFromHeader := r.Header.Get("Username")

	// TODO TODO: Add creds from cookie

	var username string
	var apikey string

	// Lets take the username in the query as priority (since new credentials would be in url)
	if usernameFromQuery != "" && usernameFromHeader == "" {
		username = usernameFromQuery
	} else {
		username = usernameFromHeader
	}

	// Lets take the apikey in the query as priority
	if apikeyFromQuery != "" && apikeyFromHeader == "" {
		apikey = apikeyFromQuery
	} else {
		apikey = apikeyFromHeader
	}

	token := tokenFromHeader
	password := passwordFromHeader

	return UserCredentials{username, password, token, apikey}
}

func getURLAction(reqCtx context.Context, r *http.Request) context.Context {

	/* Returns action from URL */

	// Lets get all mux variables
	vars := mux.Vars(r)
	// And pull the action var
	action := vars["action"]

	// We'll return the new context
	return context.WithValue(reqCtx, "action", action)
}

func getURLObject(reqCtx context.Context, r *http.Request) context.Context {

	/* Returns object from URL */

	// Lets get all mux variables
	vars := mux.Vars(r)
	// And pull the action var
	object := vars["object"]

	// We'll return the new context
	return context.WithValue(reqCtx, "object", object)
}
