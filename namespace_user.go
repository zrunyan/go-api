package main

import (
	"database/sql"
	"net/http"

	"context"

	"bitbucket.com/zrunyan/go-api/indexes"
	"bitbucket.com/zrunyan/go-api/routes"
	"bitbucket.com/zrunyan/go-logger"
)

type UserNSRequest struct {
	ctx      context.Context
	Logger   *goLogger.Logger
	Config   Config
	PsqlConn *sql.DB
}

type Url struct {
	event  string
	object string
}

func (uh *UserNSRequest) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	/*** All requests for the user namespace pass through here ***/

	// Lets get some request information and add to request scoped context
	reqCtxAction := getURLAction(r.Context(), r)
	reqCtxObject := getURLObject(r.Context(), r)

	// Assign
	reqUrl := Url{indexes.UrlActionFromContext(reqCtxAction), indexes.UrlObjectFromContext(reqCtxObject)}

	/* Any atypical requests not following /user/action/object route
	   needs to be handled here */

	/* Action Switch */
	switch reqUrl.event {
	case "test":
		routes.Test(uh.ctx, w, r)

	}
}

func NewUserNSRequest(ctx context.Context) *UserNSRequest {

	uh := &UserNSRequest{
		ctx:      ctx,
		Logger:   ctx.Value("logger").(*goLogger.Logger),
		Config:   ctx.Value("config").(Config),
		PsqlConn: ctx.Value("psqlConn").(*sql.DB),
	}

	return uh
}
