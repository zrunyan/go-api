package indexes

import (
	"context"

	"bitbucket.com/zrunyan/go-logger"
)

/**
 Context Index --- Allows access to all request scoped contexts
**/

// Gets the logger context
func LoggerFromContext(ctx context.Context) *goLogger.Logger {
	return ctx.Value("logger").(*goLogger.Logger)
}

// Gets the request Id scoped to the request
func ReqIdFromcontext(ctx context.Context) string {
	return ctx.Value("ReqID").(string)
}

// Gets the tokenUser from context
func TokenUserFromContext(ctx context.Context) TokenUser {
	return ctx.Value("tokenUser").(TokenUser)
}

func UrlActionFromContext(ctx context.Context) string {
	return ctx.Value("action").(string)
}

func UrlObjectFromContext(ctx context.Context) string {
	return ctx.Value("object").(string)
}
