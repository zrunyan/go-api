package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"os"

	"bitbucket.com/zrunyan/go-logger"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	// "sync"
)

func main() {

	/* Configuration */

	var config Config
	var err error

	// Reads in config file
	// Exits on error (unable to start api if no config)
	if ok, err := ReadInConfigFile(&config); !ok {

		//Warn the user that we cannot start up
		fmt.Println("There was an error reading in the config file, exiting now", err)
		os.Exit(1)
	}

	/* Logger */

	//Lets create a logger that we are able to pass around in the context
	Logger, err := goLogger.NewLogger(config.LogFile)
	if err != nil {

		fmt.Println("There was an error creating a logger instance, exiting now", err)
		os.Exit(1)
	}

	/* Database Connections */

	psqlConn, err := sql.Open("postgres", config.PsqlDBUser+":"+config.PsqlDBPassword+"@("+
		config.PsqlDBURL+":"+config.PsqlDBPort+")/"+config.PsqlDBName+"?parseTime=true")
	if err != nil {
		Logger.Fatal("Could not connect to the database, exiting now", err)
		os.Exit(1)
	}

	// err = psqlConn.Ping()
	// // Check connection after opening db conn, exit if err
	// if err != nil {
	// 	Logger.Fatal("Could not connect to the Postgres database with error: ", err)
	// 	os.Exit(1)
	// }

	/* Context */

	// Anything scoped to entire API should go here.
	// EACH namespace can have access to these contexts
	// AS WELL as cross package access
	ctx := context.Background()
	ctx = context.WithValue(ctx, "config", config)
	ctx = context.WithValue(ctx, "logger", Logger)
	ctx = context.WithValue(ctx, "psqlConn", psqlConn)

	/* Http Routing*/

	// New instance of gorilla mux router
	router := mux.NewRouter().StrictSlash(true)

	// v1Router for api/v1 prefix
	v1Router := router.PathPrefix("/api/v1").Subrouter()

	/* Namespaces */

	// // Core Namespace -- namespace for all requests affecting core functionality
	// v1Core := v1Router.PathPrefix("/core").Subrouter()
	// // Creates context for the core request
	// ch := newCoreRequest(ctx)

	// User Handler -- namespace for all requests scoped to a single user
	v1User := v1Router.PathPrefix("/user").Subrouter()
	// Creates context for the user request
	uh := NewUserNSRequest(ctx)

	/* Handlers */

	// // Core Handler
	// v1Core.Handle("/", uh)
	// v1Core.Handle("/{action}", uh)
	// v1Core.Handle("/{action}/{object}", uh)

	// User Handler
	v1User.Handle("/", uh)
	v1User.Handle("/{action}", uh)
	v1User.Handle("/{action}/{object}", uh)

	/* Http Router */

	Logger.Info("Starting API")
	err = http.ListenAndServe("localhost:3767", RequestId(ctx, Authentication(ctx, v1Router)))
	if err != nil {
		fmt.Println("Error starting api router: ", err)
		os.Exit(1)
	}
}
