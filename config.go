package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
)

//Config is the overall app configuration change this struct as we change the config file
type Config struct {
	LogFile        string // The file to which the api should write its logs to
	PsqlDBUser     string // The Postgres DB Username
	PsqlDBPassword string // The Postgres DB Password
	PsqlDBURL      string // the Postgres DB URL
	PsqlDBPort     string // The Postgres DB Port
	PsqlDBName     string // The Postgres DB name
}

//ReadInConfigFile will take the json config file and put it into the config struct
func ReadInConfigFile(config *Config) (ok bool, err error) {

	//Lets get the config file from the flags. If not there just get from this directory
	configFile := flag.String("config", "./config.json", "Sets the config for this service")

	//Parse the command line flags that were sent to us
	flag.Parse()

	//Create a reference to the file
	file, err := ioutil.ReadFile(*configFile)

	if err != nil {
		return
	}

	//Read the file into the config struct
	if err = json.Unmarshal(file, config); err != nil {
		return
	}

	ok = true

	return
}
