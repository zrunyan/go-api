package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"bitbucket.com/zrunyan/go-api/indexes"
	"bitbucket.com/zrunyan/go-logger"
	"github.com/dchest/uniuri"
)

type NewRequest struct {
	ctx      context.Context
	Logger   *goLogger.Logger
	Config   Config
	PsqlConn *sql.DB
}

// Request Identifier middleware

func RequestId(ctx context.Context, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Lets add the Request Id to context so we can pass this around
		ReqCtx := newContextWithRequestID(r.Context(), r)

		Logger := indexes.LoggerFromContext(ctx)
		ReqID := indexes.ReqIdFromcontext(ReqCtx)

		// Log request Id
		Logger.Info("New request received: ", ReqID)

		// Lets dump the entire request in debug
		reqInfo := dumpRequestInfo(ctx, r)
		// Log to debug
		Logger.Debug(ReqID, ": ", string(reqInfo))

		// Next middleware
		next.ServeHTTP(w, r.WithContext(ReqCtx))
	})
}

func newContextWithRequestID(ReqCtx context.Context, req *http.Request) context.Context {

	// Lets generate a random Id here
	timeInNano := time.Now().UnixNano()
	hash := uniuri.NewLen(4)
	ReqID := fmt.Sprintf("%v%v", timeInNano, hash)

	return context.WithValue(ReqCtx, "ReqID", ReqID)
}
